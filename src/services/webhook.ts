import { config } from "../config.js";
import { IEvent } from "../models/event.js";
import { WebHook } from "../models/webhook.js";
import { downloadURL } from "../utils/http.js";
import { decrypt } from "./encryption.js";

export async function publishWebhookEvent(
  event: IEvent,
  webhook: WebHook,
  actionType: "create" | "update",
) {
  const decryptedToken = decrypt(
    webhook.encryptedToken,
    webhook.encryptedTokenIV,
    config.ENCRYPTION_KEY,
  );
  if (!decryptedToken) {
    throw "Unable to decrypt secret token for Webhook";
  }

  await downloadURL(webhook.targetUrl, {
    method: "POST",
    headers: {
      "X-Mbz-Import-Tool-Token": decryptedToken,
    },
    body: JSON.stringify({
      event,
      actionType,
    }),
  });
}
