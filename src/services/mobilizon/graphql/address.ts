import { gql } from "graphql-request";

export const ADDRESS_FRAGMENT = gql`
  fragment AdressFragment on Address {
    id
    description
    geom
    street
    locality
    postalCode
    region
    country
    type
    url
    originId
    timezone
    pictureInfo {
      url
      author {
        name
        url
      }
      source {
        name
        url
      }
    }
  }
`;
