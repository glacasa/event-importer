import { IActor } from "./actor.js";
import { Paginate } from "./paginate.js";

export enum ROLE {
  ADMINISTRATOR = "ADMINISTRATOR",
  MODERATOR = "MODERATOR",
  MEMBER = "MEMBER",
}

export const ROLES_VALUES = {
  ADMINISTRATOR: 10,
  MODERATOR: 5,
  MEMBER: 3,
};

export interface IMember {
  id: string;
  role: ROLE;
  actor: IActor;
  parent: IActor;
  invitedBy: IActor;
}

export function deduplicateMemberships(
  memberships: Paginate<IMember>,
): Record<string, IMember> {
  return (memberships?.elements ?? []).reduce(
    (acc, membership) => {
      const existing = acc[membership.parent.url];
      if (
        !existing ||
        ROLES_VALUES[existing.role] <= ROLES_VALUES[membership.role]
      ) {
        acc[membership.parent.url] = membership;
      }
      return acc;
    },
    {} as Record<string, IMember>,
  );
}
