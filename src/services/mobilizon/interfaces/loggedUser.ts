import { IActor } from "./actor.js";
import { IMember } from "./member.js";
import { Paginate } from "./paginate.js";

export interface ILoggedUser {
  id: string;
  email: string;
  defaultActor: {
    avatar: {
      url: string;
    };
    preferredUsername: string;
    name: string;
  };
  memberships?: Paginate<IMember>;
  actors?: IActor[];
}
