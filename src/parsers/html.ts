import { load, CheerioAPI, Element } from "cheerio";

export function parseHTML(html: string): CheerioAPI {
  return load(html);
}

export function jsonLDBlocks(parsedHTML: CheerioAPI): Record<string, any>[] {
  let definitions: Record<string, any>[] = [];
  parsedHTML('script[type="application/ld+json"]').each(
    (i, rawDefinition: Element) => {
      try {
        const data = load(rawDefinition).text().trim();
        definitions.push(JSON.parse(data));
      } catch {
        console.error(`Failed parsing JSON-ld content`);
      }
    },
  );
  return definitions;
}
