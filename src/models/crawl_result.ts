import { IEvent } from "./event.js";
import { IOrganizer } from "./organizer.js";
import { SOURCE_TYPES } from "./sync.js";

export interface CrawlEventResult {
  event: IEvent | null;
  title: string | undefined;
  originUrl: string;
}

export interface CrawlOrganizerResult {
  organizer: IOrganizer;
  title: string;
  originUrl: string;
  sourceType: SOURCE_TYPES;
}

export type CrawlResult = CrawlEventResult | CrawlOrganizerResult;

export function crawlResultIsCrawlOrganizerType(
  crawlResult: CrawlResult,
): crawlResult is CrawlOrganizerResult {
  return "organizer" in crawlResult;
}
