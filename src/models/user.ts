import {
  Column,
  CreatedAt,
  DataType,
  HasMany,
  Model,
  Table,
  UpdatedAt,
} from "sequelize-typescript";
import { MobilizonOAuthApplicationToken } from "./mobilizon_oauth_application.model.js";
import { FacebookUser } from "./facebook_user.model.js";

export interface IUser {
  id: string;
  email: string;
  username: string;
  name?: string;
  avatar?: string | undefined;
}

@Table({ tableName: "users" })
export class User extends Model {
  @Column(DataType.STRING)
  declare email: string;

  @Column(DataType.STRING)
  declare name: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @HasMany(() => MobilizonOAuthApplicationToken, "userId")
  declare mobilizonOAuthApplicationTokens: MobilizonOAuthApplicationToken[];

  @HasMany(() => FacebookUser, "userId")
  declare facebookUsers: FacebookUser[];
}
