import {
  AllowNull,
  Table,
  Column,
  Model,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  HasMany,
  DataType,
} from "sequelize-typescript";
import { User } from "./user.js";
import { SyncRelation } from "./sync.js";

@Table({ tableName: "destinations" })
export class Destination extends Model {
  @Column(DataType.STRING)
  declare targetUrl: string;

  @Column(DataType.STRING)
  declare name: string;

  @Column(DataType.STRING)
  declare type: string;

  @AllowNull
  @Column(DataType.STRING)
  declare imageUrl: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @HasMany(() => SyncRelation, "destinationId")
  declare syncRelations: ReturnType<() => SyncRelation>;

  @BelongsTo(() => User, "userId")
  declare user: ReturnType<() => User>;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  declare userId: number;
}
