import {
  Table,
  Column,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  DataType,
} from "sequelize-typescript";
import { User } from "./user.js";

@Table({ tableName: "mbz_oauth_applications" })
export class MobilizonOAuthApplication extends Model {
  @Column(DataType.STRING)
  declare name: string;

  @Column(DataType.STRING)
  declare hostname: string;

  @Column(DataType.STRING)
  declare client_id: string;

  @Column(DataType.STRING)
  declare client_secret: string;

  @Column(DataType.STRING)
  declare scopes: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @HasMany(() => MobilizonOAuthApplicationToken, "appId")
  declare tokens: MobilizonOAuthApplicationToken[];
}

@Table({ tableName: "mbz_oauth_application_tokens" })
export class MobilizonOAuthApplicationToken extends Model {
  @Column(DataType.STRING)
  declare federatedIdentity: string;

  @Column(DataType.STRING)
  declare accessToken: string;

  @Column(DataType.STRING)
  declare refreshToken: string;

  @Column(DataType.STRING)
  declare scopes: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @ForeignKey(() => MobilizonOAuthApplication)
  @Column(DataType.INTEGER)
  declare appId: number;

  @BelongsTo(() => MobilizonOAuthApplication, "appId")
  declare app: MobilizonOAuthApplication;

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  declare userId: number;

  @BelongsTo(() => User, "userId")
  declare user: ReturnType<() => User>;
}
