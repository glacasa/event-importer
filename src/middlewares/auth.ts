import type { NextFunction, Request, Response } from "express";

export const checkLogin = () => {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!req.session.user) {
      return res.status(403).render("errors/403");
    }
    next();
  };
};
