import { Schema } from "./schema.js";
import { jsonLDBlocks, parseHTML } from "../parsers/html.js";
import { ImportResult } from "./importer.js";
import { EventJoinOptions, IEvent } from "../models/event.js";
import { downloadURL } from "../utils/http.js";
import { IOrganizer } from "../models/organizer.js";

export class EventBrite extends Schema {
  async importEvent(url: string): Promise<ImportResult<IEvent>> {
    const { result: event, body } = await super.importEvent(url);
    const dom = parseHTML(body);
    if (event) {
      console.debug("Improving Eventbrite event description");
      event.description =
        dom(
          ".event-details__main:last-child .event-details__section-title + div",
        ).html() ?? event.description;

      const tags: string[] = [];
      dom(".event-details__section-title + ul li.tags-item").each((_i, tag) => {
        tags.push(dom(tag).text());
      });
      event.tags = tags;

      // const eventIdMatch = url.match(/\/events\/(\d+)/)
      // if (eventIdMatch) {
      //   const eventId = eventIdMatch[1];
      //   const tags = (await this.fetchTags(eventId));
      //   console.debug('tags', tags);
      //   event.tags = tags.event.topics.edges.map(({ node: { name} }) => name);
      // }
    }
    return { result: event, body };
  }

  async importEvents(url: string): Promise<ImportResult<IOrganizer>> {
    const groupPageBody = await (await downloadURL(url)).text();
    const groupPageDom = parseHTML(groupPageBody);
    const jsonLD = jsonLDBlocks(groupPageDom).flat();
    const organization = jsonLD.find(
      (elem) => elem["@type"] === "Organization",
    );
    const eventElementUrls = jsonLD
      .filter((elem) => elem["@type"] === "Event")
      .map((event) => event.url);
    if (groupPageDom(".organizer-profile__section").toArray().length === 0) {
      throw "URL is not a group page";
    }
    const result = (await Promise.all(
      [...new Set(eventElementUrls)]
        .map(async (url) => (await this.importEvent(url)).result)
        .filter(async (url) => (await url) !== null),
    )) as IEvent[];
    return Promise.resolve({
      result: {
        events: result
          .filter((event) => event)
          .filter((event) => event.startDate > new Date()),
        name: organization?.name ?? "Unknown organizer",
        url,
        image: organization?.logo,
        description: organization?.description,
      },
      body: groupPageBody,
    });
  }
}
