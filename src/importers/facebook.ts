import {
  EventAttendanceMode,
  EventJoinOptions,
  IEvent,
} from "../models/event.js";
import { EventStatus } from "../models/event.js";
import { downloadURL } from "../utils/http.js";
import { ImportResult, Importer } from "./importer.js";
// @ts-ignore
import { scrapeFbEvent } from "facebook-event-scraper";
import linkifyStr from "linkify-string";

export class Facebook implements Importer {
  async importEvent(url: string): Promise<ImportResult<IEvent>> {
    try {
      const eventData = await scrapeFbEvent(url);
      console.debug("Found eventData from Facebook", eventData);
      const startDate = new Date();
      startDate.setTime(eventData.startTimestamp * 1000);
      let endDate;
      if (eventData.endTimestamp) {
        endDate = new Date();
        endDate.setTime(eventData.endTimestamp * 1000);
      }

      const geom = eventData.location?.coordinates
        ? `${eventData.location.coordinates.longitude};${eventData.location.coordinates.latitude}`
        : null;

      const result: IEvent = {
        name: eventData.name,
        description: this.htmlizeDescription(eventData.description),
        image: eventData.photo
          ? (await this.directPhotoURL(eventData.photo?.url)) ?? ""
          : "",
        startDate,
        endDate,
        organizer: {
          type: eventData?.hosts[0].type === "User" ? "Person" : "Organization",
          name: eventData?.hosts[0].name,
          url: eventData?.hosts[0].url,
        },
        url: eventData.url,
        eventStatus: EventStatus.SCHEDULED,
        eventAttendanceMode:
          eventData?.isOnline === true
            ? EventAttendanceMode.ONLINE
            : EventAttendanceMode.OFFLINE,
        tags: [],
      };

      if (eventData.location?.name) {
        result.location = {
          name: eventData.location?.name ?? null,
          address: {
            geom: geom,
            addressLocality: eventData.location?.city?.name ?? null,
            addressRegion: null,
            addressCountry: null,
            streetAddress: eventData.location?.address,
          },
        };
      }

      if (eventData.ticketUrl) {
        result.joinOptions = EventJoinOptions.EXTERNAL;
        result.externalParticipationUrl = eventData.ticketUrl;
      }

      return {
        result,
        body: "",
      };
    } catch (err) {
      console.error(err);
    }

    return { result: null, body: "" };
  }

  private async directPhotoURL(photoURL: string): Promise<string | undefined> {
    const photoHTML = await (
      await downloadURL(photoURL, {
        headers: {
          Accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/jxl,image/webp,*/*;q=0.8",
        },
      })
    ).text();
    // console.debug(photoHTML);
    const matches = photoHTML.match(
      /"image":{"uri":"(.+)","width":.+,"height":.+},"accessibility_caption"/,
    );
    if (matches) {
      return matches[1].replace(/\\\//g, "/");
    }
    return undefined;
  }

  private htmlizeDescription(description: string): string {
    return this.newLineToParagraph(description);
  }

  private newLineToParagraph(string: string): string {
    let paragraphs = "";
    string.split("\n\n").forEach((line) => {
      if (line.trim()) {
        paragraphs += "<p>" + linkifyStr(line).replace(/\n/g, "<br>") + "</p>";
      }
    });
    return paragraphs;
  }
}
