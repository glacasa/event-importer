import { Request } from "express";
import { config } from "./config.js";

export const isSecureRequest = (req: Request) => req.protocol === "https";

export const isRunningOnHttps = () =>
  ["80", "443"].includes(config.WEB_PORT.toString());

const portSuffix = () => {
  if (!isRunningOnHttps()) {
    return `:${config.WEB_PORT}`;
  }
  return "";
};

export const endpointUrl = (req: Request) =>
  `${req.protocol}://${req.hostname}${portSuffix()}`;

export const httpProtocol = () => (isRunningOnHttps() ? "https" : "http");
