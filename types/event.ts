export interface ImportedEvent {
  id: string;
  sourceUrl: string;
  body?: any;
}
